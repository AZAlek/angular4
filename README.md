by Aleksey Zhadeev
for Front-End Frameworks: Angular 4.0 by Hong Kong University of Science and Technology (Coursera.org).
---

Prerequisites:

!IMPORTANT!- Node version >8.1<9 must be running, application absolutely will not run on node 6!
To download and install node, please follow instructions here: https://nodejs.org/dist/v8.9.4

Project Description

The project consists of two modules:  Front-end application, as well as json-server that acts as a dummy back-end.
-Front-end application requires Angular CLI. Install dependencies from package.json (command prompt-> npm install)
-To install json-server -> npm install json-server -g.
 
Once installed:

-run json-server before starting app server -> json-server --watch db.json (will run mock back-end on localhost:3000)
-run app server -> ng serve --open (will open app on localhost:4200 in default browser)
